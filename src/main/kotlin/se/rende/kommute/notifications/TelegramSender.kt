package se.rende.kommute.notifications

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import mu.KLogging
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import java.io.ByteArrayOutputStream
import java.nio.charset.Charset

class TelegramSender(
        private val apiKey: String = System.getenv("TELEGRAM_API_KEY"),
        private val chatId: String = System.getenv("TELEGRAM_CHAT_ID"),
        private val telegramUrl: String = "https://api.telegram.org"
): Sender, KLogging() {
    private val objectMapper = ObjectMapper().registerModule(KotlinModule())
    private val utf8 = Charset.forName("utf-8")

    override fun sendMessage(message: String) {
        val request = HttpPost("$telegramUrl/bot$apiKey/sendMessage")
        request.addHeader("Content-Type", "application/json")
        request.entity = StringEntity(objectMapper.writeValueAsString(mapOf("chat_id" to chatId, "text" to message)), utf8)
        HttpClients.createDefault().use {
            it.execute(request).use { response ->
                val responseBody = convertResponseToString(response)
                if (response.statusLine.statusCode != 200) {
                    logger.error("Got non-OK response from Telegram: ${response.statusLine} $responseBody")
                    throw RuntimeException("Telegram API error")
                }

                val parsedResponse: Map<String, String> = objectMapper.readValue(responseBody)
                if (parsedResponse["ok"]?.toBoolean() != true) {
                    logger.error("Got non-OK response from Telegram: $responseBody")
                    throw RuntimeException("Telegram API error")
                } else {
                    logger.info("Successfully sent message to Telegram")
                }
            }
        }
    }

    private fun convertResponseToString(response: CloseableHttpResponse): String {
        try {
            ByteArrayOutputStream().use {
                response.entity.writeTo(it)
                return String(it.toByteArray())
            }
        } catch (e: Exception) {
            return ""
        }
    }
}