package se.rende.kommute.notifications

import com.commit451.mailgun.Contact
import com.commit451.mailgun.Mailgun
import com.commit451.mailgun.SendMessageRequest

class EmailSender (private val domain: String = System.getProperty("MAILGUN_DOMAIN"),
                   private val mailgun: Mailgun = Mailgun.Builder(domain, System.getProperty("MAILGUN_API_KEY")).build()): Sender {

    override fun sendMessage(message: String) {
        val from = Contact("notification@mailgun.com", "kommute-notifications")
        val to = listOf(Contact("jim@example.com", "jim"))

        val requestBuilder = SendMessageRequest.Builder(from)
                .to(to)
                .subject("Commute problem detected")
                .text(message)

        val response = mailgun.sendMessage(requestBuilder.build())
                .blockingGet()
        println("Response id ${response.id}, message ${response.message}")
    }
}