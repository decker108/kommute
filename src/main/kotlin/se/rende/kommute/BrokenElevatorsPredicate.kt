package se.rende.kommute

val brokenElevators = { td: TrafficDeviation ->
    listOf(td.Details.orEmpty(), td.Header.orEmpty())
            .any { Regex("[Hh]iss(en|ar|arna|)").containsMatchIn(it) }
}