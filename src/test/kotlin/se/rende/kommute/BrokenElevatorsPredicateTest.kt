package se.rende.kommute

import org.junit.Test
import kotlin.test.assertNotNull

class BrokenElevatorsPredicateTest {
    @Test
    fun shouldFindBrokenElevatorWithUppercaseInitials() {
        val result = listOf(makeTrafDev("Hissen avstängd vid T-Centralen"))
                .find(brokenElevators)

        assertNotNull(result)
    }

    @Test
    fun shouldFindBrokenElevatorWithLowercaseInitials() {
        val result = listOf(makeTrafDev( "Avstängd hiss vid T-Centralen"))
                .find(brokenElevators)

        assertNotNull(result)
    }

    @Test
    fun shouldFindBrokenElevatorWithUppercaseInitialsInDetails() {
        val result = listOf(makeTrafDev("example", "Hissen avstängd vid T-Centralen"))
                .find(brokenElevators)

        assertNotNull(result)
    }

    @Test
    fun shouldFindBrokenElevatorWithLowercaseInitialsInDetails() {
        val result = listOf(makeTrafDev("example", "Avstängd hiss vid T-Centralen"))
                .find(brokenElevators)

        assertNotNull(result)
    }

    @Test
    fun shouldFindBrokenElevatosrWithUppercaseInitials() {
        val result = listOf(makeTrafDev("Hissen avstängd vid T-Centralen"))
                .find(brokenElevators)

        assertNotNull(result)
    }

    @Test
    fun shouldFindBrokenElevatorsWithLowercaseInitials() {
        val result = listOf(makeTrafDev( "Avstängd hiss vid T-Centralen"))
                .find(brokenElevators)

        assertNotNull(result)
    }

    @Test
    fun shouldFindBrokenElevatorsWithUppercaseInitialsInDetails() {
        val result = listOf(makeTrafDev("example", "Hissen avstängd vid T-Centralen"))
                .find(brokenElevators)

        assertNotNull(result)
    }

    @Test
    fun shouldFindBrokenElevatorsWithLowercaseInitialsInDetails() {
        val result = listOf(makeTrafDev("example", "Avstängd hiss vid T-Centralen"))
                .find(brokenElevators)

        assertNotNull(result)
    }

    private fun makeTrafDev(header: String, details: String = "example"): TrafficDeviation {
        return TrafficDeviation("123", "1970-01-01", header, details, "example",
                "example", "123", "456", "true")
    }
}