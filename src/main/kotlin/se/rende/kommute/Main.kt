package se.rende.kommute

import mu.KLogger
import mu.KLogging
import se.rende.kommute.notifications.Sender
import se.rende.kommute.notifications.TelegramSender

fun main() {
    val logger = KLogging().logger()
    logger.info("Checking for env vars")
    checkEnvVars({ envVarName: String -> System.getenv(envVarName) }, logger)
    logger.info("Checking for deviations...")
    try {
        checkForDeviations(getDeviationsInfo(), TelegramSender(), System.getenv("KOMMUTE_IGNORED_GIDS_FILEPATH")!!, logger)
    } catch (e: Exception) {
        logger.error("Unexpected error", e)
    }
    logger.info("Done checking")
}

fun checkEnvVars(getEnvVar: (String) -> String?, logger: KLogger) {
    for (envVarName in listOf("KOMMUTE_IGNORED_GIDS_FILEPATH", "TRAFIKLAB_API_KEY", "TELEGRAM_API_KEY", "TELEGRAM_CHAT_ID")) {
        if (getEnvVar(envVarName).isNullOrEmpty()) {
            logger.error("Required env var $envVarName not set, aborting...")
            throw IllegalStateException("Required env var $envVarName not set, aborting...")
        }
    }
}

fun checkForDeviations(deviations: TrafikLabResponse, sender: Sender, ignoredGidsFilePath: String, logger: KLogger) {
    deviations.ResponseData?.let {
        it
                .filterNot(createIgnoredDeviationGidsPredicate(ignoredGidsFilePath))
                .filterNot(brokenElevators)
                .ifEmpty {
                    logger.info("Found ${if (it.isEmpty()) "no" else it.size} deviations" +
                            "${if (it.isNotEmpty()) ", but all were marked as ignorable" else ""}.")
                    return
                }
                .forEach { td ->
                    val summary = "${td.Header} \n${td.Details} \n${td.Scope}"
                    sender.sendMessage("Warning, commute is in danger: $summary")
                    logger.info("Trafiklab response: $td")
                }
    }
}