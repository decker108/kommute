# Kommute

A commute problem warner service written in Kotlin. Uses email, sms,
Telegram or Signal for notifications. Scheduled by crontab.

Uses Trafiklab's Deviations API for data.

    http://api.sl.se/api2/deviations.json?key=API_KEY&LineNumber=LINE_NUMBER
