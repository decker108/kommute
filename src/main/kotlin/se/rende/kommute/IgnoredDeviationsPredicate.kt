package se.rende.kommute

import org.apache.commons.io.FileUtils

fun createIgnoredDeviationGidsPredicate(ignoredGidsFilePath: String?): (TrafficDeviation) -> Boolean {
    val ignoredGidsFile = FileUtils.getFile(ignoredGidsFilePath)
    val ignoredGids = FileUtils.readLines(ignoredGidsFile, "utf8")
            .filter { !it.isNullOrEmpty() }
            .map { java.lang.Long.parseLong(it) }
    return { ignoredGids.contains(it.DevCaseGid?.toLong()) }
}