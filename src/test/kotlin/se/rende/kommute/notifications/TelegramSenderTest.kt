package se.rende.kommute.notifications

import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import com.github.tomakehurst.wiremock.junit.WireMockRule
import org.junit.Rule
import org.junit.Test
import kotlin.test.assertFails

class TelegramSenderTest {
    @JvmField
    @Rule
    public val wireMockRule = WireMockRule(options().port(13337))

    @Test
    fun shouldThrowErrorIfTelegramReturnsNon200Response() {
        wireMockRule.stubFor(post(urlPathMatching("/botexample/sendMessage"))
                .withHeader("Content-Type", equalTo("application/json"))
                .withRequestBody(equalTo("{\"chat_id\":\"12345\",\"text\":\"example message\"}"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"ok\":false}")))
        val sender = TelegramSender("example", "12345", "http://localhost:13337")

        assertFails("Telegram API error") { sender.sendMessage("example message") }

        wireMockRule.verify(1, postRequestedFor(urlPathMatching("/botexample/sendMessage"))
                .withHeader("Content-Type", equalTo("application/json"))
                .withRequestBody(equalTo("{\"chat_id\":\"12345\",\"text\":\"example message\"}")))
    }

    @Test
    fun shouldThrowErrorIfTelegramReturnsNon200ResponseWithStringBoolean() {
        wireMockRule.stubFor(post(urlPathMatching("/botexample/sendMessage"))
                .withHeader("Content-Type", equalTo("application/json"))
                .withRequestBody(equalTo("{\"chat_id\":\"12345\",\"text\":\"example message\"}"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"ok\":\"false\"}")))
        val sender = TelegramSender("example", "12345", "http://localhost:13337")

        assertFails("Telegram API error") { sender.sendMessage("example message") }

        wireMockRule.verify(1, postRequestedFor(urlPathMatching("/botexample/sendMessage")))
    }

    @Test
    fun shouldReturnUnitIfCallIsSuccessful() {
        wireMockRule.stubFor(post(urlPathMatching("/botexample/sendMessage"))
                .withHeader("Content-Type", equalTo("application/json"))
                .withRequestBody(equalTo("{\"chat_id\":\"12345\",\"text\":\"example message\"}"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"ok\":true}")))
        val sender = TelegramSender("example", "12345", "http://localhost:13337")

        sender.sendMessage("example message")

        wireMockRule.verify(1, postRequestedFor(urlPathMatching("/botexample/sendMessage")))
    }
}