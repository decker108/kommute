package se.rende.kommute

import org.junit.Test
import se.rende.kommute.notifications.Sender
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertTrue

class MainTest {

    private val blockingSender: Sender = object : Sender {
        override fun sendMessage(message: String) {
            throw IllegalStateException("Should not be called")
        }
    }

    @Test
    fun shouldNotSendMessageIfDeviationsAreEmpty() {
        val mockLogger = MockKLogger()
        val deviations = listOf<TrafficDeviation>()
        val mockTrafikLabResponse = TrafikLabResponse(1000, "", null, deviations)

        checkForDeviations(mockTrafikLabResponse, blockingSender, "/dev/null", mockLogger)

        assertTrue { mockLogger.infoLogs.contains("Found no deviations.") }
    }

    @Test
    fun shouldThrowExceptionIfEnvVarIsMissing() {
        val mockLogger = MockKLogger()
        val errMsg = "Required env var KOMMUTE_IGNORED_GIDS_FILEPATH not set, aborting..."

        assertFails(errMsg) { checkEnvVars({ null }, mockLogger) }

        assertTrue { mockLogger.errorLogs.contains<Pair<String, Throwable?>>(Pair(errMsg, null)) }
    }

    @Test
    fun shouldNotSendMessageIfAllDeviationsAreIgnorable() {
        val ignoredGidsFilePath = this.javaClass.getResource("/ignoredGids").path
        val mockLogger = MockKLogger()
        val deviations = listOf(TrafficDeviation("9076001016340608", "1970-01-01T00:00:00", "Test header", "Test detauls", "Test scope", "Test scopeElems", "yesterday", "today", "just now"),
                TrafficDeviation("12345", "1970-01-01T00:00:00", "Test header", "Avstängda hissar vid Odenplan", "Test scope", "Test scopeElems", "yesterday", "today", "just now"))
        val mockTrafikLabResponse = TrafikLabResponse(1000, "", null, deviations)

        checkForDeviations(mockTrafikLabResponse, blockingSender, ignoredGidsFilePath, mockLogger)

        assertEquals(listOf("Found 2 deviations, but all were marked as ignorable."), mockLogger.infoLogs)
    }

    @Test
    fun shouldSendMessageIfResponseContainsNonIgnorableDeviations() {
        val ignoredGidsFilePath = this.javaClass.getResource("/ignoredGids").path
        val mockLogger = MockKLogger()
        val actualDeviation = TrafficDeviation("12345", "1970-01-01T00:00:00", "Test header", "Inställd trafik pga allmänt kaos", "Alla linjer", "Test scopeElems", "yesterday", "today", "just now")
        val deviations = listOf(TrafficDeviation("9076001016340608", "1970-01-01T00:00:00", "Test header", "Test detauls", "Test scope", "Test scopeElems", "yesterday", "today", "just now"),
                actualDeviation)
        val mockTrafikLabResponse = TrafikLabResponse(1000, "", null, deviations)
        var sentMessage = ""
        val mockSender = object : Sender {
            override fun sendMessage(message: String) {
                sentMessage = message
            }
        }
        checkForDeviations(mockTrafikLabResponse, mockSender, ignoredGidsFilePath, mockLogger)

        assertEquals(listOf("Trafiklab response: $actualDeviation"), mockLogger.infoLogs)
        assertEquals("Warning, commute is in danger: Test header \n" +
                "Inställd trafik pga allmänt kaos \n" +
                "Alla linjer", sentMessage)
    }
}
