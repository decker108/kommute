package se.rende.kommute.notifications

import com.twilio.Twilio
import com.twilio.rest.api.v2010.account.Message
import com.twilio.type.PhoneNumber

class SmsSender(private val ACCOUNT_SID: String = System.getProperty("TWILIO_ACCOUNT_SID"),
                private val AUTH_TOKEN: String = System.getProperty("TWILIO_AUTH_TOKEN")): Sender {

    override fun sendMessage(message: String) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN)

        val recipients = listOf(System.getProperty("RECIPIENT_PHONE_NUM"))
        val sender = System.getProperty("TWILIO_SENDER_PHONE_NUM")

        recipients.forEach { recipient ->
            val smsMessage = Message
                    .creator(PhoneNumber(recipient),
                            PhoneNumber(sender),
                            message)
                    .create()
            println(smsMessage.sid)
        }
    }
}