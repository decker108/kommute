package se.rende.kommute

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class TrafficDeviation(
        val DevCaseGid: String?,
        val Created: String?,
        val Header: String?,
        val Details: String?,
        val Scope: String?,
        val ScopeElements: String?,
        val FromDateTime: String?,
        val UpToDateTime: String?,
        val Updated: String?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class TrafikLabResponse(
        val StatusCode: Int?,
        val Message: String?,
        @JsonIgnore val ExecutionTime: Unit?,
        val ResponseData: List<TrafficDeviation>?
)