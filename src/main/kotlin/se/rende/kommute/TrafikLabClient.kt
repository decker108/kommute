package se.rende.kommute

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import mu.KLogging
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClients
import java.io.ByteArrayOutputStream

private val API_KEY = System.getenv("TRAFIKLAB_API_KEY")

private fun parseResponse(jsonResponse: String): TrafikLabResponse {
    val objectMapper = ObjectMapper()
            .registerModule(Jdk8Module())
            .registerModule(JavaTimeModule())
            .registerModule(KotlinModule())
            .enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
            .enable(SerializationFeature.WRITE_DATES_WITH_ZONE_ID)

    return objectMapper.readValue(jsonResponse)
}

fun getDeviationsInfo(): TrafikLabResponse {
    val logger = KLogging().logger()
    HttpClients.createDefault().use { client ->
//        val siteId = 9109 // Seems to filter out important info affecting all the line?
        val lineNumber = "17,18,19"
//        val url = "http://api.sl.se/api2/deviations.json?key=$API_KEY&SiteId=$siteId&LineNumber=$lineNumber"
        val url = "http://api.sl.se/api2/deviations.json?key=$API_KEY&LineNumber=$lineNumber"
        logger.info("Calling TrafikLab API with with url: $url")
        val httpGet = HttpGet(url)
        client.execute(httpGet).use { r ->
            if (r.statusLine.statusCode == 200) {
                val response = parseResponse(ByteArrayOutputStream().use { baos ->
                    r.entity.writeTo(baos)
                    String(baos.toByteArray())
                })
                if (response.StatusCode != 0) {
                    throw IllegalStateException("TrafikLab API error: ${response.StatusCode} ${response.Message}, url was $url")
                }
                return response
            } else {
                val errorMsg = "Error calling Trafiklab: ${r.statusLine.statusCode} ${r.statusLine.reasonPhrase}"
                throw IllegalStateException(errorMsg)
            }
        }
    }
}