package se.rende.kommute.notifications

interface Sender {
    fun sendMessage(message: String)
}
